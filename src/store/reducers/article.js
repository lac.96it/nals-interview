import { createSlice } from '@reduxjs/toolkit'

const articleSlice = createSlice({
    name: 'posts',
    initialState: {
        listArticle: [],
        pagination: {
            page: 1,
            limit: 10,
            sortBy: undefined,
            order: undefined,
            search: undefined,
            filter: undefined,
            title: undefined,
            total: 0
        }
    },
    reducers: {
        getListArticleSuccess(state, action) {
            state.listArticle = action.payload;
        },
        getListArticleFail(state, action) {
            state.listArticle = [];
        },
        setPagination(state, action) {
            state.pagination = {...state.pagination, ...action.payload};
        }
    }
})

// Extract the action creators object and the reducer
const { actions, reducer } = articleSlice
// Extract and export each action creator by name
export const { getListArticleSuccess, getListArticleFail, setPagination } = actions
// Export the reducer, either as a default or named export
export default reducer