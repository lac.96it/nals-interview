import { configureStore } from '@reduxjs/toolkit';
import articleReducer from './reducers/article';

const store = configureStore({
    reducer: {
        article: articleReducer
    }
})

export default store