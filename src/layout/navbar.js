import React, { useState } from 'react';
import { Link } from 'react-router-dom';

const Navbar = () => {

    const [show, setShow] = useState(false);

    const toggleShowNavs = () => setShow(!show);

    return (
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand" href="/">
                <img src="https://getbootstrap.com/docs/4.5/assets/brand/bootstrap-solid.svg" width={30} height={30} className="d-inline-block align-top mr-1" alt="" loading="lazy" />
                NALS TEST
            </a>
            <button onClick={toggleShowNavs} class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class={`collapse navbar-collapse ${show ? 'show' : ''}`} id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <Link className="nav-link" to='/home/articles'>Articles</Link>
                </div>
            </div>
        </nav>
    )
}

export default Navbar;