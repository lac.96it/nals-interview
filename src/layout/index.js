import React from 'react';

import Navbar from './navbar';

const BasicLayout = ({ children }) => {
    return (
        <div>
            <Navbar />
            <div className='container'>
                {children}
            </div>
        </div>
    )
}

export default BasicLayout;