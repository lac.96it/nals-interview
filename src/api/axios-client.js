import axios from 'axios';
import queryString from 'query-string';

const axiosClient = axios.create({
    headers: {
        'content-type': 'application/json',
    },
    paramsSerializer: (params) => queryString.stringify(params),
});

axiosClient.interceptors.response.use(
    (response) => {

        if (response.status === 404) throw new Error('Not found');
        if (response.status === 500) throw new Error('Server Error');

        if (response && response.data) {
            return response.data;
        }

        return response;
    },
    (error) => {
        // Handle errors
        throw error;
    }
);

export default axiosClient;
