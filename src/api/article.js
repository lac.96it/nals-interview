import axios from './axios-client';

export const getListArticle = ({
    page,
    limit,
    sortBy,
    order,
    search,
    filter,
    title
}) => {

    return axios({
        method: 'GET',
        url: 'https://5f55a98f39221c00167fb11a.mockapi.io/blogs',
        params: {
            page,
            limit,
            sortBy,
            order,
            search,
            filter,
            title
        }
    });
}

export const getArticle = (articleId) => {
    return axios({
        method: 'GET',
        url: `https://5f55a98f39221c00167fb11a.mockapi.io/blogs/${articleId}`,
    });
}