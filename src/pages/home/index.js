import React, { Suspense, lazy, useEffect } from 'react';
import { Switch, Route, Redirect, useRouteMatch } from 'react-router-dom';
import { Loading } from '../../components';
import BasicLayout from '../../layout';

const ListArticlePage = lazy(() => import('../list-article'));
const DetailArticlePage = lazy(() => import('../detail-article'));

const HomePage = () => {
    const { path } = useRouteMatch();

    useEffect(() => {
        document.title = 'NALS Test'
    }, []);

    return (
        <BasicLayout>
            <Suspense fallback={<Loading />}>
                <Switch>
                    <Redirect exact from={path} to={`${path}/articles`} />

                    <Route path={`${path}/articles/:id`} component={DetailArticlePage} />

                    <Route path={`${path}/articles`} component={ListArticlePage} />

                    <Redirect to="/404" />
                </Switch>
            </Suspense>
        </BasicLayout>
    )
}

export default HomePage;