import React from 'react';
import { useSelector } from 'react-redux';
import { Loading, EmptyData } from '../../components';
import {useHistory} from 'react-router-dom';

const ListArticleMedia = ({loading}) => {
    const history = useHistory();

    const listArticle = useSelector((state) => state.article.listArticle);

    const handleClickArticle = (id) => history.push(`/home/articles/${id}`);

    if(loading) return <Loading />;

    if(!listArticle.length) return <EmptyData />;

    return (
        <ul className="list-unstyled article__list">
            {listArticle.map(({ title, image, content, id }, idx) => (
                <li className={`media${(idx > 0 && idx < 9) ? ' my-4' : ''}`} key={`media-${id}`} onClick={() => handleClickArticle(id)}>
                    <img src={image} className="mr-3 article__img" alt={title} />
                    <div className="media-body">
                        <h5 className="mt-0 mb-1">{title}</h5>
                        {content}
                    </div>
                </li>
            ))}
        </ul>
    )
}

export default ListArticleMedia;