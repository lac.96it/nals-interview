import React from 'react';
import { debounce } from 'lodash';
import { useSelector } from 'react-redux';

const ListArticleActions = ({ onChange }) => {

    const pagination = useSelector(state => state.article.pagination);

    const handleChangeSearchText = debounce((e) => {
        onChange({ search: e.target.value });
    }, 700);

    const handleSortChange = (e) => {
        if (e.target.value === 'undefined') onChange({ sortBy: undefined });
        else {
            if (!pagination.orderBy) onChange({ sortBy: e.target.value, order: 'asc' });
            else onChange({ sortBy: e.target.value });
        }
    }

    const handleOrderChange = (e) => {
        if (e.target.value === 'undefined') onChange({ order: undefined });
        else onChange({ order: e.target.value });
    }

    return (
        <div className="my-3 d-flex article-actions">
            <div className="input-group">
                <input
                    onChange={handleChangeSearchText}
                    type="text"
                    className="form-control"
                    placeholder="Search articles"
                    aria-label="Search articles"
                    aria-describedby="basic-addon1"
                />
            </div>
            <div className="input-group">
                <select className="custom-select" value={pagination.sortBy} onChange={handleSortChange}>
                    <option value={'undefined'}>SortBy</option>
                    <option value={'title'}>Title</option>
                    <option value={'content'}>Content</option>
                    <option value={'createdAt'}>Created At</option>
                </select>
                <div className="input-group-append">
                    <select className="custom-select" value={pagination.order} onChange={handleOrderChange}>
                        <option value={'undefined'}>Order</option>
                        <option value={'asc'}>Ascend</option>
                        <option value={'desc'}>Descend</option>
                    </select>
                </div>
            </div>
        </div>
    )
}

export default ListArticleActions;