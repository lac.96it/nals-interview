import React, { useCallback, useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getListArticleSuccess, getListArticleFail, setPagination } from '../../store/reducers/article';
import * as articleApi from '../../api/article';

import { Pagination } from '../../components';
import ListArticleMedia from './list-media';
import ListArticleActions from './list-actions';

import './index.scss';

const ListArticlePage = () => {

    const [loading, setLoading] = useState(false);
    const pagination = useSelector((state) => state.article.pagination);

    const dispatch = useDispatch();

    useEffect(() => {
        getTotalArticles();
        document.title = 'Articles';

        // eslint-disable-next-line
    }, []);

    useEffect(() => {
        getListArticle();
        // eslint-disable-next-line
    }, [pagination.page, pagination.pageSize, pagination.sortBy, pagination.order, pagination.search, pagination.filter, pagination.title]);

    const getListArticle = useCallback(() => {
        setLoading(true);
        articleApi.getListArticle(pagination)
            .then((listArticle) => {
                dispatch(getListArticleSuccess(listArticle));
            }).catch(() => {
                dispatch(getListArticleFail());
            }).finally(() => setLoading(false));

        // eslint-disable-next-line
    }, [pagination]);

    const getTotalArticles = useCallback(() => {
        articleApi.getListArticle({})
            .then((listArticle) => {
                dispatch(setPagination({ total: listArticle.length }));
            }).catch(() => {
                dispatch(setPagination({ total: 0 }));
            })

        // eslint-disable-next-line
    }, []);

    const paginationChange = (newPagination) => {
        dispatch(setPagination(newPagination));
    }

    return (
        <div>
            <ListArticleActions onChange={paginationChange} />
            <ListArticleMedia loading={loading} />
            <Pagination
                page={pagination.page}
                pageSize={pagination.limit}
                total={pagination.total}
                onChange={(page) => paginationChange({ page })}
            />
        </div>
    )
}

export default ListArticlePage;