import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { getArticle } from '../../api/article';
import moment from 'moment';

import { Loading, EmptyData } from '../../components';

import './index.scss';

const DetailArticle = () => {

    const [article, setArticle] = useState();
    const [loading, setLoading] = useState(false);
    const params = useParams();

    useEffect(() => {
        if (params.id) {
            setLoading(true);

            getArticle(params.id)
                .then(article => {
                    setArticle(article)
                })
                .catch(() => {
                    console.log('cant get data');
                })
                .finally(() => setLoading(false));
        }

        // eslint-disable-next-line
    }, []);

    if (loading) return <Loading />;

    if (!article) return <EmptyData />;

    return (
        <div className="article-detail">
            <h2 className="article-detail__title">{article.title}</h2>
            <ul className="article-detail__meta">
                <li className="article-detail__pushlish">{moment(article.createdAt).format('DD/MM/YYYY HH:mm')}</li>
            </ul>
            <img className="article-detail__img" src={article.image} alt="Article" />

            <p className="article-detail__content">{article.content}</p>
        </div>
    )
}

export default DetailArticle;