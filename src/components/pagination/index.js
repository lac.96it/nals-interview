import React, { useMemo } from 'react';
import { range } from 'lodash';

const Pagination = ({ page, pageSize, total, onChange }) => {

    const numberOfPage = useMemo(() => {
        return Math.ceil(total / pageSize);
    }, [total, pageSize]);

    const goPrev = () => onChange(page - 1);
    const goNext = () => onChange(page + 1);
    const changePage = (idx) => {
        if (idx === page) return;

        onChange(idx);
    }

    if (total === 0) return <></>;

    return (
        <nav aria-label="...">
            <ul className="pagination">
                <li className={`page-item ${page === 1 ? 'disabled' : ''}`}>
                    <a
                        onClick={goPrev}
                        className="page-link"
                        href="javascript:void(0)"
                        tabIndex={page === 1 ? -1 : 0}
                        aria-disabled={page === 1 ? 'true' : 'false'}
                    >Previous</a>
                </li>
                {range(numberOfPage).map(idx => (
                    <li className={`page-item ${idx + 1 === page ? 'active' : ''}`} key={`pagination-item-${idx}`}>
                        <a onClick={() => changePage(idx + 1)} className="page-link" href="javascript:void(0)">{idx + 1}</a>
                    </li>
                ))}
                <li className={`page-item ${page === numberOfPage ? 'disabled' : ''}`}>
                    <a
                        className="page-link"
                        href="javascript:void(0)"
                        aria-disabled={page === numberOfPage ? 'true' : 'false'}
                        onClick={goNext}
                    >Next</a>
                </li>
            </ul>
        </nav>
    )
}

export default Pagination;