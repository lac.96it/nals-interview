import React from 'react';

import './index.scss';

const EmptyData = () => {
    return (
        <div className="empty-data">
            <span>Empty data</span>
        </div>
    )
}

export default EmptyData;