import React from 'react';

import './index.scss';

const Loading = () => {
    return (
        <div className='loading-wrapper'>
            <div className="loading spinner-border text-primary loading" role="status">
                <span className="sr-only">Loading...</span>
            </div>
        </div>
    )
}

export default Loading;