import React, { lazy, Suspense } from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { Loading } from '../components';

const HomePage = lazy(() => import('../pages/home'));
const Page404 = lazy(() => import('../pages/404'));

const Routes = () => {
    return (
        <Suspense fallback={<Loading />}>
            <BrowserRouter>
                <Switch>
                    <Redirect exact from='/' to='/home' />

                    <Route path='/home' component={HomePage} />
                    <Route path='/404' component={Page404} />

                    <Redirect to='/404' />
                </Switch>
            </BrowserRouter>
        </Suspense>
    )
}

export default Routes;